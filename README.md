# dotfiles

These are my personal dotfiles. I use chezmoi to manage them. So I don't forget, here is how to install them on a Mac.

1. Install Homebrew

	```bash
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	```

2. Install gpg

	```bash
	brew install --cask gpg-suite-no-mail
	```

3. Install git

	```bash
	brew install git
	```

4. Import keys from flash drive into gpg

5. Install ssh keys from flash drive.

	```bash
	cd
	cp -R /Volumes/flashdrive/.ssh .
	```

6. Clone and install dotfiles.

	```bash
	"${HOME}/bin/chezmoi" init git@gitlab.com:StoneyJackson/dotfiles.git --apply
	```
