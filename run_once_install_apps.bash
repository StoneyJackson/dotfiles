#!/bin/bash

# Install Homebrew

if ! command -v brew &> /dev/null
then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
else
    brew upgrade
fi

# Generate Brewfile for `brew bundle install`
# Q: Why not keep a dot_Brewfile?
# A: Because we want chezmoi to rerun this script to rerun if the
#    package list in this file is modified.
BREWFILE="${HOME}/.Brewfile"
cat > "${BREWFILE}" <<EOF
# Do not modify this file.
# Edit chezmoi/run_once_install_apps.bash instead.
# This file can be used to upgrade your apps: brew bundle --global install

tap "beeftornado/rmtree"
tap "homebrew/cask"
tap "federico-terzi/espanso"

brew "antigen"
brew "chezmoi"
brew "espanso"
brew "git"
brew "mas"
brew "neovim"
brew "the_silver_searcher"
brew "tree"
brew "whalebrew"
brew "zsh"

cask "adobe-acrobat-reader"
cask "caffeine"
cask "discord"
cask "docker"
cask "freemind"
cask "grandperspective"
cask "google-chrome"
cask "gpg-suite-no-mail"
cask "iterm2"
cask "keycastr"
cask "rectangle"
cask "slack"
cask "vscodium"
cask "zoom"

mas "AuthPass", id: 1478552452
EOF

# Install everything
brew bundle --file="${BREWFILE}" --verbose install

# Make Google Chrome the default browser
open -a "Google Chrome" --args --make-default-browser

# Set global gitignore.
git config --global core.excludesfile ~/.config/git/gitignore

# Install Python environment
# pyenv install 3.8.5 && pyenv global 3.8.5
# if ! python --version | grep 3.8.5 > /dev/null
# then
#     >&2 echo 'ERROR: Python did not install completely'
#     >&2 echo 'ERROR: Python 3.8.5 is not in the path.'
#     >&2 echo 'ERROR: Maybe add `eval "$(pyenv init -)"` to the end of .zshrc.'
# else
#     python -m pip install --upgrade pip
#     python -m pip install --user pipx
#     pipx install poetry
# fi

# Install VS Codium Extensions
code --install-extension ban.spellright
